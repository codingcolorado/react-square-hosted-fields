# React Square Hosted Fields
A react component for Square's hosted fields

## What this Is
A quick way of plugging in your App ID to Square's hosted payment fields in a react environment.

## Getting Started
You can get started with the React payment form in a few easy steps:

`npm install --save react-square-hosted-fields`

## Simple Usage

Import the payment form:

`import SquarePaymentForm from 'react-square-hosted-fields';`

Put the payment form anywhere:

`<SquarePaymentForm appId="YOUR-SQUARE-APPLICATION-ID" onNonceGenerated={this.handleNonce} />`

## Using a GET Resource

You can either plug in your application id directly via the `appId` prop as it's done above, or use the `get` prop to specify a get resource.

`<SquarePaymentForm get="/some/api/path" onNonceGenerated={this.handleNonce} onGetAppIdError={this.handleGetError} />`

Don't forget to handle an error with your resource! The component expects the resource to return a json object like this:

```
{ appId: 'YOUR-SQUARE-APPLICATION-ID' }
```

## Square Options

A majority of the options match what goes into [Square's SqPaymentForm](https://docs.connect.squareup.com/articles/adding-payment-form). See the docs for more clarification there.

Prop                        | Type      | Default                   | Description
--------------------------- | -------   | ------------------------- | ---------------------------------------------------------------------------------
appId                       | String    | `''` (Required ifn get)   | Your [Square Application ID](https://connect.squareup.com/apps)
get                         | String    | `''` (Required ifn appId) | URL or path to a GET resouce, handled with promises
submitText                  | String    | `Submit Payment`          | The text that shows up on the submit button to generate a nonce
inputClass                  | String    | `sq-input`                | The class name(s) that will be put into iframe elements for styling the inputs
inputStyles                 | Array     | `[]`                      | Array of input style objects as per SqPaymentForm docs
cardNumber.elementId        | String    | `sq-card-number`          | The element id that SqPaymentForm will replace with the card number field
cardNumber.placeholder      | String    | `E.g. XXXX-XXXX-XXXX-XXXX`| The placeholder text for the card number field once replaced by SqPaymentForm
cvv.elementId               | String    | `sq-cvv`                  | The element id that SqPaymentForm will replace with the CVV field
cvv.placeholder             | String    | `E.g. XXX`                | The placeholder text for the CVV field once replaced by SqPaymentForm
expirationDate.elementId    | String    | `sq-expiration-date`      | The element id that SqPaymentForm will replace with the expiration date field
expirationDate.placeholder  | String    | `MM/YY`                   | The placeholder text for the expiration date field once replaced by SqPaymentForm
postalCode.elementId        | String    | `sq-postal-code`          | The element id that SqPaymentForm will replace with the postal code field
postalCode.placeholder      | String    | `E.g. XXXXX`              | The placeholder text for the postal code field once replaced by SqPaymentFor

## Callbacks

The component comes with a few callbacks that you can use to loading or user events with the form. See [Square's SqPaymentForm](https://docs.connect.squareup.com/articles/adding-payment-form) for more details on `SqPaymentForm`

Prop                          | Parameters                    | Fires
----------------------------- | ----------------------------- | ------------------------------------------------------------------------
onGetAppIdError               | Object [axiosError](https://github.com/mzabriskie/axios#handling-errors)             | If the `get` prop is specified and the API resource returns an error code
onScriptCreated               |                               | As soon as the SqPaymentForm client library script tags are created and put into the DOM
onScriptError                 |                               | When the SqPaymentForm client library encounters an error while loading
onScriptLoaded                |                               | When the SqPaymentForm client library is finished loading and ready for use
onPaymentFormLoaded           |                               | After a new instance of SqPaymentForm is created
onInputEventReceived          | String inputEvent             | Whenever a user does something to the payment form, Square fires their own [inputEvent]((https://docs.connect.squareup.com/articles/adding-payment-form) object
onUnsupportedBrowserDetected  |                               | If SqPaymentForm has determined the user's browser is not supported
onNonceRequested              |                               | As soon as the user clicks the submit button for the form (throws an error if it happens before the payment form is generated)
onNonceGenerated              | String nonce, Object cardData | When a payment nonce has been generated successfully
onNonceError                  | Array errors                  | When Square reports an error while generating a payment nonce

## Styling Options

There are also a few options specific to this package that can help style the form. Note that no styling is done on the form other than what Square does by default.

Prop                        | Type      | Default                   | Description
--------------------------- | -------   | ------------------------- | -------------------------------------------------------------------------------------------------------
componentWrapperClass       | String    | `sq-component-wrapper`    | Class name(s) for the div that wraps the entire component
formWrapperClass            | String    | `sq-form-wrapper`         | Class name(s) for the div that wraps the entire form
fieldsWrapperClass          | String    | `sq-fields-wrapper`       | Class name(s) for the div that wraps only the input fields (not the submit button)
buttonWrapperClass          | String    | `sq-button-wrapper`       | Class name(s) for the div that wraps only the submit button
inputWrapperClass           | String    | `sq-input-wrapper`        | Class name(s) for the div that wraps each input field (iframe)
cardWrapperClass            | String    | `sq-card-wrapper`         | Class name(s) for the div that wraps the card input field (would override inputWrapperClass)
cvvWrapperClass             | String    | `sq-cvv-wrapper`          | Class name(s) for the div that wraps the cvv input field (would override inputWrapperClass)
expirationWrapperClass      | String    | `sq-expiration-wrapper`   | Class name(s) for the div that wraps the expiration date input field (would override inputWrapperClass)
postalWrapperClass          | String    | `sq-postal-wrapper`       | Class name(s) for the div that wraps the postal code input field (would override inputWrapperClass)

The submit button can be styled using `.buttonWrapperClass button`, as it's simply a button element.

## Using Custom Elements

The component also supports the ability to use children within the component itself if you already have a payment form styled.

```
<SquarePaymentForm get="/some/api/resource" onNonceGenerated={this.handleNonce}>
  <div className="some-custom-wrapper">
    <CustomWrapper>
      <CustomComponent>Card Number</CustomComponent>
      <div className="my-own-class" id="sq-card-number"></div>
    </CustomWrapper>

    <CustomWrapper>
      <CustomComponent>Card Number</CustomComponent>
      <div className="my-own-class" id="sq-cvv"></div>
    </CustomWrapper>
    
    <CustomWrapper>
      <CustomComponent>Card Number</CustomComponent>
      <div className="my-own-class" id="sq-expiration-date"></div>
    </CustomWrapper>
    
    <CustomWrapper>
      <CustomComponent>Card Number</CustomComponent>
      <div className="my-own-class" id="sq-postal-code"></div>
    </CustomWrapper>
  </div>
</SquarePaymentForm>
```

Do not forget to include the id props for your divs and make sure they match with the props elementId props you send into the component (the defaults are used in the example). If you don't, SqPaymentForm will not be able to find the divs to replace with iframes.

Note that the submit payment button will be included as part of the component. If you have a way around this, please [suggest it](https://bitbucket.org/codingcolorado/react-square-hosted-fields/issues).

## More Information

If you see something you think is missing or would like to contribute, please check out the [Bitbucket repo](https://bitbucket.org/codingcolorado/react-square-hosted-fields/)

